#I "/Program Files (x86)/NUnit.org/framework/3.4.1.0/net-4.5"
#r "nunit.framework.dll"

#load "HelloWorld.fs"
#load "HelloWorldTest.fs"

open HelloWorld
open HelloWorldTest
open NUnit.Framework

``No name`` ()
``Sample name`` ()
``Other sample name`` ()

// fsc -a -I "/Program Files (x86)/NUnit.org/framework/3.4.1.0/net-4.5" HelloWorld.fs HelloWorldTest.fs