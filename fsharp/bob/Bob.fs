module Bob

open System

let isAlpha (input:string) = String.exists Char.IsLetter input

let (|Yell|_|) input = if isAlpha input && input = input.ToUpper() 
                       then Some(input) else None

let (|Empty|_|) (input:string) = if input.Trim() = "" then Some(input) else None

let hey (say:string) =
  match say with
  | Empty s -> "Fine. Be that way!"
  | Yell s -> "Whoa, chill out!"
  | q when q.EndsWith("?") -> "Sure."
  | _ -> "Whatever."
