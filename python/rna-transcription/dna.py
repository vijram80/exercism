_d = dict(zip('GCAT', 'CGUA'))

def to_rna(dna):
    return "".join(map(_d.get, dna))