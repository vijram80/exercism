module Meetup (Weekday(..), Schedule(..), meetupDay) where

import Data.Time.Calendar (Day, addDays, fromGregorian, gregorianMonthLength)
import Data.Time.Calendar.OrdinalDate (sundayStartWeek)

data Schedule = First | Second | Third | Fourth
  | Last | Teenth deriving (Eq, Enum, Ord)

data Weekday = Sunday | Monday | Tuesday | Wednesday | Thursday
  | Friday | Saturday deriving (Eq, Enum, Show)

-- The task is to create the data types `Weekday` and
-- `Schedule`, and implement the function `meetupDay`.

meetupDay :: Schedule -> Weekday -> Integer -> Int -> Day
meetupDay sched weekday year month
  | sched < Last = addDays (7 * weekDayNum) (findDayBy succ (fg 1) weekday)
  | sched == Last = findDayBy pred (fg lastDay) weekday
  | otherwise = findDayBy succ (fg 13) weekday
  where fg = fromGregorian year month
        lastDay = gregorianMonthLength year month
        weekDayNum = fromIntegral (fromEnum sched)

dayOfWeek :: Day -> Weekday
dayOfWeek = toEnum . snd . sundayStartWeek

findDayBy :: (Day -> Day) -> Day -> Weekday -> Day
findDayBy f day weekday
  | dayOfWeek day == weekday = day
  | otherwise = findDayBy f (f day) weekday
