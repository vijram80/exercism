module Scrabble (scoreLetter, scoreWord) where

import Data.Char (toUpper)

scoreLetter :: Char -> Int
scoreLetter x = scores !! (fromEnum (toUpper x) - 65) where
  scores = [1,3,3,2,1,4,2,4,1,8,5,1,3,1,1,3,10,1,1,1,1,4,4,8,4,10]

scoreWord :: String -> Int
scoreWord = sum . map scoreLetter
