module SumOfMultiples where

sumOfMultiples :: [Int] -> Int -> Int
sumOfMultiples nums to = sum $ foldr mults [] [1..to - 1] where
  mults n acc
    | any (\x -> n `mod` x == 0) nums = n : acc
    | otherwise = acc
