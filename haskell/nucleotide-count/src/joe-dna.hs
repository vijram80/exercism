module JoeDNA (count, nucleotideCounts) where

import qualified Data.Map as M

nucleotideMap = M.fromList [('A', 0),('C', 0),('G', 0),('T', 0)]

count :: Char -> String -> Either String Int
count c s
    | invalidNChar c = Left ("invalid nucleotide " ++ show c)
    | invalidNString s /= ' ' = Left ("invalid nucleotide " ++ show (invalidNString s))
    | otherwise = Right $ unsafeCount c s

invalidNChar :: Char -> Bool
invalidNChar c = M.notMember c nucleotideMap

invalidNString :: String -> Char
invalidNString s
    | not $ null badList = head badList
    | otherwise = ' '
    where badList = [ x | x <- s, M.notMember x nucleotideMap]

unsafeCount :: Char -> String -> Int
unsafeCount c s =  length (filter (== c) s)

nucleotideCounts :: String -> Either String (M.Map Char Int)
nucleotideCounts s
    | invalidNString s /= ' ' = Left ("invalid nucleotide " ++ show (invalidNString s))
    | otherwise = Right (M.insert 'T' ( unsafeCount 'T' s) $
                         M.insert 'G' ( unsafeCount 'G' s) $
                         M.insert 'C' ( unsafeCount 'C' s) $
                         M.insert 'A' ( unsafeCount 'A' s) nucleotideMap)
