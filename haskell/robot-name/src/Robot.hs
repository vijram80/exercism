module Robot (Robot, mkRobot, resetName, robotName) where

import System.Random
import Data.IORef

-- The task is to create the data type `Robot`, as a
-- mutable variable, and implement the functions below.

type Robot = IORef String

mkRobot :: IO Robot
mkRobot = do
  name <- getName
  newIORef name

resetName :: Robot -> IO ()
resetName r = do
  name <- getName
  writeIORef r name

robotName :: Robot -> IO String
robotName = readIORef

getName :: IO String
getName = do
  g <- newStdGen
  let ints = randomRs (0, 9) g :: [Int]
  let chars = randomRs ('A', 'Z') g
  return $ take 2 chars ++ foldr1 (++) (map show $ take 3 ints)
