open System

let volume r h =
  Math.PI * r * r * h

let getVal s =
  printf "%s: " s
  float (Console.ReadLine())

[<EntryPoint>]
let main argv =
  let r = getVal "Radius"
  let h = getVal "Height"
  printfn "%f" (volume r h)
  0

//volume 10.0 9.0

open System

let isAlpha (s:string) = String.forall Char.IsLetter s

let (|Yell|_|) input = if isAlpha input && input = input.ToUpper()
                       then Some(input) else None

let hey (say:string) =
  match say with
  | "" -> "Fine. Be that way!"
  | Yell s -> "Whoa, chill out!"
  | _ -> "Whatever."

hey ""
