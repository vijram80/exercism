type person = {
    name: string;
    age : int
}

type foo = {
    name: string;
    age : int
}

let f = fun (p:person) -> p.name

// [<EntryPointAttribute>]
// let main args =
//     let dave = { name="jack"; age=10 } : person
//     let n = f dave
//     0

//------------------------------------------

let foo1 = [1;2;3;4]
let foo2 = [|5;6;7;8|]
let foo3 = seq [9;10;11;12]
printfn "%A" (List.take 2 foo1)
printfn "%A" (Array.take 2 foo2)
printfn "%A" (Seq.take 2 foo3)

(List.take 2 << List.tail) foo1
(List.take 2 >> List.tail) foo1
foo1 |> List.tail |> List.take 2



let isGreaterThanTen twotuple =
    match twotuple with
    | (x,_) when x > 10 -> true
    | (x,_) when x <= 10 && x > 0   -> false
    | (_, 10) -> true
    | _ -> false

isGreaterThanTen (2,3)

let mytest a b =
    match a, b with
    | 10, _ -> true
    | _ -> false

mytest 10 4

let keepIfPositive (a : int) =
    if a > 0 then Some(a)
    else None

let check a =
    match keepIfPositive a with
        | Some(a) -> true
        | _ -> false

let(|IsOdd|_|) input = if input % 2 <> 0 then Some() else None

let(|IsLessThan30|_|) input = if input < 30 then Some() else None

let checknumber x =
    match x with
    | IsOdd & IsLessThan30 -> "This candidate is ideal"
    | IsOdd -> "Is Odd and slightlight ideal"
    | IsLessThan30 -> "Is less than ideal"
    | _ -> "This item is not ideal"

checknumber 5

// setup the active patterns
let (|MultOf3|_|) i = if i % 3 = 0 then Some MultOf3 else None
let (|MultOf5|_|) i = if i % 5 = 0 then Some MultOf5 else None
// the main function
let fizzBuzz i =
    match i with
    | MultOf3 & MultOf5 -> printf "FizzBuzz, "
    | MultOf3 -> printf "Fizz, "
    | MultOf5 -> printf "Buzz, "
    | _ -> printf "%i, " i
// test
[1..20] |> List.iter fizzBuzz

type Foo = Foo|Bar|Baz
let (|Small|Big|_|) input = if input < 100 then Some Small elif input > 1000 then Some Big else None


// let test input =
//     match input with
//     | Positive -> 1
//     | Negetive -> -1
//     | Zero -> 0

type TrafficLight = Green | Yellow | Red


let changeLight light =
  match light with
  | Green -> Yellow
  | Yellow -> Red;
  | Red -> Green

type ChangeLight = TrafficLight -> TrafficLight
ChangeLight
let (f l : ChangeLight) = Yellow

open System
let foo (input:string) =
  match input with
  | _ when input.ToUpper() = input -> "UP"
  | _ when String.forall Char.IsLower input -> "DOWN"
  | _ -> "MiX"

foo "abc"
